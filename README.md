# README #

I wanted to build a cheap and reliable motion sensor for a long time to connect to my homeassistant as all the motion sensors available in the market are either zwave based, or does not work with homeassistant or they were very expensive.
So I built a mqtt based motion sensor with Wemos D1 Mini and PIR SR501 motion sensor which works flawlessly on wifi.

### What is this repository for? ###

* This repo is for poeple looking to bulid a rapid prototype of a wifi based motion sensor with esp8266 (Wemos D1 Mini in my case) + PIR SR 501 sensor + mqtt + homeassistant
* I hacked this code by looking at some examples and some basic knowledge of Aurduino, dont expect a great code, but it works. Feel free to raise pull requests to enhance it further.
* Once you put this code on the Wemos D1 Mini, you will be able to flash it over the wifi itself so that you dont have to take out the sensor again and again to update it. I have taken the sample code of basic OTA and implemented it.
* Ver 0.1

### How do I get set up? ###

* Hardware requirements:
	Wemos D1 Mini/ Node MCU / any esp8266 based board
	HC-SR501 PIR Module
* Software requirements
	Arduino IDE
	Running homeassistant instance
	Running mqtt server	
* Dependencies
	Board Manager for the Wemos D1 Mini: http://arduino.esp8266.com/stable/package_esp8266com_index.json
	Pubsub Arduino Library

### TODO ###

* Web based configuration for easy use.
* Homeassistant AutoDiscovery

### Contribution guidelines ###

* You are free to raise a pull request to enhance the features.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact