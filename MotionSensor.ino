// Author: Vishal Pal Chaudhary
// Bitbucket: https://bitbucket.org/vishix

#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

const char* ssid = "Your SSID";
const char* password = "Your password";

#define mqtt_server "MQTT Server hostname / IP"
const int mqtt_port = 1883;
#define mqtt_user "MQTT Username"
#define mqtt_password "MQTT Password"
#define sensor_name "Your_sensor_name"  //Must be unique (Fall back already handled with chip id)- Vishal

int pirPin = D6; // Put the data pin number where u connected the pir sensor. D6 in my case
int val; 
int state = 0;
String wemos_hostname;
String mqtt_state_topic;
String mqtt_availability_topic;
String mqtt_config_topic;
WiFiClient espClient;
PubSubClient client(espClient);

void setup()
{
  Serial.begin(115200);
  // OTA CODE //
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }
  wemos_hostname = sensor_name "-" + String(ESP.getChipId(), HEX);
  ArduinoOTA.setHostname(String(wemos_hostname).c_str());
  
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // OTA CODE //

  mqtt_config_topic="homeassistant/binary_sensor/" sensor_name "/config";
  mqtt_state_topic="homeassistant/binary_sensor/" sensor_name "/state";
  mqtt_availability_topic="homeassistant/binary_sensor/" sensor_name;
  
  //wemos_hostname += String(ESP.getChipId(), HEX);
  Serial.println("hostname="+wemos_hostname);
  pinMode(LED_BUILTIN, OUTPUT);
//  delay(5000);
  Serial.println();
//  WiFi.printDiag(Serial);
  Serial.println(WiFi.localIP());
  client.setServer(mqtt_server, mqtt_port);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
    char will_message [] = "offline";
    if (client.connect(String(wemos_hostname).c_str(), mqtt_user, mqtt_password,String(mqtt_availability_topic).c_str(),0,1,will_message)) {
      client.publish(String(mqtt_config_topic).c_str(), String("{\"name\":\"" sensor_name "\", \"device_class\": \"motion\"}").c_str(), true); //Autodiscovery packet sent with retained flag.
      client.publish(String(mqtt_availability_topic).c_str(), String("online").c_str(), true);
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void publish_message(int state){
  //Serial.println("/home/" sensor_name "/motion");
  if(state)
    client.publish(String(mqtt_state_topic).c_str(), String("ON").c_str(), true);
  else
    client.publish(String(mqtt_state_topic).c_str(), String("OFF").c_str(), true);
}
 
void loop()
{
  ArduinoOTA.handle();
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  val = digitalRead(pirPin);
  //low = no motion, high = motion
  if (val == LOW)
  { 
    //Serial.println(state);
    if(state == 1)
    { 
      state = 0;
      publish_message(state);
      //client.publish("/home/"+sensor_name+"/motion", String(state).c_str(), true);
      Serial.println("No motion");
      pinMode(LED_BUILTIN, 0);
      
    }
  }
  else
  {
    //Serial.println(state);
    if(state == 0)
    {
      state = 1;
      publish_message(state);
      Serial.println("Motion detected  ALARM");
      pinMode(LED_BUILTIN, 1);
    }
  }
  
  delay(1);
}